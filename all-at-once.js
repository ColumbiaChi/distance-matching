//* -------------------------------------------
// ## change some varialbes and run this file from mongo in the terminal: "mongo localhost:27017/geomatcher all-at-once.js"
//-------------------------------------------  */

// what collection do you want to update with matches(e.g. "newinq")?
var mainCollection = "newinq";

// what collection do you want to find matches in (e.g, "sessions")?
var sessionsCollection = "sessions1314";





// Locfield expects "Latitude" and "Longitude" to be fields in the main collection
load('locfield.js');

/* -------------------------------------------
Calculate-distance will match sessions to the main collection.

Currently expects the following fields:
--------------------------------------------
 Location
 Date
 Venue
 addr
 "Start Time"
 -------------------------------------------*/

load('calculate-distance.js');


load('600only.js');

print('...success!');