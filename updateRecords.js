/*
Going through a set of new or updated records to update the main database.
set up an array / object that has all modified records. here, we're grabbing this from a temporary "newRecs" database
*/
var updatedRecords = db.newRecs.find().toArray();


for (var i=0, len = updatedRecords.length; i<len; i++) {
    db.inq.update({"Number" : updatedRecords[i]["Number"]},{$set : {
            lat : updatedRecords[i]["Lat"],
            lon: updatedRecords[i]["Lon"],
            Address: updatedRecords[i]["Address"],
            session1: null,
            session2: null
        }
    });
}

