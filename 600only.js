// make sure all sessions are (re)indexed for geospatial queries before running everything else
// db.completeSessions.ensureIndex({loc:"2d"});
db.columbia.ensureIndex({loc:"2d"});
/*
## MEAT AND POTATOES

*/

// setup earth radius for distance calculations, and the range that we are interested in
var earthRadius = 3959; // radius of the earth in miles
var range = 250; // we are looking at sessions within a 50 mile radius a prospective student


//create an array with everything from db.example
var arr = db.newinq.find().toArray();


for (var i=0, len = arr.length; i < len; i++) {
    var lon = arr[i].loc.lon;
    var lat = arr[i].loc.lat;
    print( i + ': ' + arr[i].loc.lat );
    // nearestSession = db.runCommand({ geoNear : "completeSessions", near : [lon,lat], num : 1 /*limit to one*/, spherical : true}).results;
    // var session1 = nearestSession[0].obj.location;
    // var miles1     = Math.round(nearestSession[0].dis * earthRadius);
    distanceFromColumbia = db.runCommand({ geoNear : "columbia", near : [lon,lat], spherical : true}).results;
    var columbiaMiles     = Math.round(distanceFromColumbia[0].dis * earthRadius);
    // print('colum miles' + columbiaMiles);
    db.newinq.update({"_id" : arr[i]["_id"]}, { $set: {
            // session1 : session1,
            // sessionAddress1 : sessionAddress1,
            // miles1: miles1,
            columbiaMiles: columbiaMiles
        }} );
 }
