var citySlicker = "";
var earthRadius = 3959; // radius of the earth in miles
var range = 50; // we are looking at sessions within a 50 mile radius a prospective student
// We've got some functions

//count an object.
function objectLength(obj) {
  var result = 0;
  for(var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
    // or Object.prototype.hasOwnProperty.call(obj, prop)
      result++;
    }
  }
  return result;
}

/*
Meat and Potatoes
*/
db.sessions.ensureIndex({loc:"2d"});


//a radius query
var earthRadius = 3959; // radius of the earth in miles
var range = 50; // we are looking at sessions within a 50 mile radius a prospective student

distances = db.runCommand({ geoNear : "sessions", near : [-149.458123,61.608579], spherical : true, maxDistance : range / earthRadius /* to radians */ }).results;
pointDistance = distances[0].dis * earthRadius; // back to miles


var lon = "";
var lat = "";
//create an array with everything from db.prosp
var arr = db.prosp.find().toArray();
//count the array
var count = arr.length;
// get the closest session for each db.prosp entry with loc.Lat/Lon!
for (var i=0; i < count; i++) {
   //print('The closest session to ' + arr[i]["First Name"] + " " + arr[i]["Last Name"] + " is: ");
   var lon = arr[i].loc.Lon;
   var lat = arr[i].loc.Lat;
   //print(lon + "," + lat)
	distances = db.runCommand({ geoNear : "sessions", near : [lon,lat], spherical : true, maxDistance : range / earthRadius /* to radians */ }).results;

	if (objectLength(distances) >= 2) {
		print("2 or more!");
		pointDistance = distances[0].dis * earthRadius; // back to miles
		// for (var j=0; j < 2; j++) {
		// print(distances[j].obj.name + " is about " + distances[j].dis * earthRadius + " miles away");
			
		//	var miles = distances[j].dis * earthRadius
		//	var nearbyCity = distances[j].obj.name
		//	var nearestSession = "nearestSession" + j;
			
		//	db.prosp.update({"Last Name" : arr[i]["Last Name"]}, { $push: {nearestSession  : nearbyCity}} );
		//	db.prosp.update({"Last Name" : arr[i]["Last Name"]}, { $set: {milesAway : miles}} );
		// }
		//use this method if you can't use varialbe names to create a new field field
		var session1 = distances[0].obj.name;
		var session2 = distances[1].obj.name;
		var miles1   = distances[0].dis * earthRadius;
		var miles2   = distances[1].dis * earthRadius;

		//UDPATE STUFF
		db.prosp.update({"_id" : arr[i]["_id"]}, { $set: {session1 : session1, session2 : session2, miles1 : miles1, miles2 : miles2} /*{miles1 : miles1}, {miles2 : miles2} */} );
		print(miles1 + " " + miles2);
	} else {
		print("only 1 :(");
		nearestSession = db.runCommand({ geoNear : "sessions", near : [lon,lat], num : 1 /*limit to one*/, spherical : true}).results;
		print(nearestSession[0].obj.name + " is about " + nearestSession[0].dis * earthRadius + " miles away");
		db.prosp.update({"_id" : arr[i]["_id"]}, { $set: {session1 : nearestSession[0].obj.name}} );
	}
}

//changed lock on "last name" to lock onto "_id"
