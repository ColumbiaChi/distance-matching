
//set up a function to print the cursor...now using builtin printjson
function printResult (r) {
  print(tojson(r))
}

function printjson (x) {
  print(tojson(x));
}

//use the findCity function with forEech to print
function findCity (z) {
  var citySlicker = z.City;
  //print(citySlicker);
  db.prosp.update({"Last Name" : arr[i]["Last Name"]}, { $set: {nearestSession: citySlicker}} );
}

function backUpCity (y) {
    var citySlicker = y.City;
  //print(citySlicker);
  db.prosp.update({"Last Name" : arr[i]["Last Name"]}, { $set: {backUpCity: citySlicker}} );
}






//db.sessions.find({loc: {$near:[arr[i].loc.Lon,arr[i].loc.Lat]}}).limit(1).forEach(printResult)




//count the number of students
function objectLength(obj) {
  var result = 0;
  for(var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
    // or Object.prototype.hasOwnProperty.call(obj, prop)
      result++;
    }
  }
  return result;
}

//create an array with everything from db.prosp
var arr = db.prosp.find().toArray();

var count = objectLength(arr);
// get the closest session for each db.prosp entry with loc.Lat/Lon!
for (var i=0; i < count; i++) {
   print('The closest session to ' + arr[i]["First Name"] + " " + arr[i]["Last Name"] + " is: ");
   //db.sessions.find( {loc: { $near:[ arr[i].loc.Lon, arr[i].loc.Lat ] } } ).limit(2).forEach(printjson);
   var citySlicker = db.sessions.find( {loc: { $nearSphere:[ arr[i].loc.Lon, arr[i].loc.Lat ] } } ).limit(1).forEach(findCity);
   //db.prosp.update({"Last Name" : arr[i]["Last Name"]}, { $set: {nearestSession: citySlicker}} );
   var citySlicker = db.sessions.find( {loc: { $nearSphere:[ arr[i].loc.Lon, arr[i].loc.Lat ] } } ).limit(2).forEach(backUpCity);

}


// db.sessions.find({loc: {$near:[arr[1].loc.Lon,arr[1].loc.Lat]}}).limit(1);
// db.sessions.find({loc: {$near:[arr[2].loc.Lon,arr[2].loc.Lat]}}).limit(1);
// db.sessions.find({loc: {$near:[arr[3].loc.Lon,arr[3].loc.Lat]}}).limit(1);

db.sessions.find({loc: {$near:[-87.627724,41.87125]}}).limit(1);

// full geoNear search
db.runCommand({geoNear:"sessions", near:[-87.627724,41.87125]});

//a radius query
var earthRadius = 3959; // radius of the earth in miles
var range = 50; // we are looking at sessions within a 50 mile radius a prospective student

distances = db.runCommand({ geoNear : "sessions", near : [-87.627724,41.87125], spherical : true, maxDistance : range / earthRadius /* to radians */ }).results;
pointDistance = distances[0].dis * earthRadius // back to miles



db.runCommand({ geoNear : "sessions", near : [-87.627724,41.87125], num : 1 /*limit to one*/, spherical : true}).results;

