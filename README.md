# README

## DISTANCE MATCHING
Scripts for matching individual geo location data in a mongodb to the closest 1 or two from another set of events.

If more than one event is within 50 miles of a person, the closest two will be appended to the individual's document.
Otherwise, only the closest event will be appeneded to the document.

------------------------------------------------------------------

## NEW IN VERSION 2
Using the `all-at-once.js` file (info below) should get you geo-matching in just three steps! You can use either a local or remote database.

**1) import**

local:

	mongoimport --db temp --collection example --type csv --file test-geo-db.csv --headerline
remote:

	mongoimport -h ds037637.mongolab.com:37637 -d geomatcher -c <collection> -u <user> -p <password> --file <input .csv file> --type csv --headerline

**2) set variables and process all-at-once.js**

local:

	mongo localhost:27017/geomatcher all-at-once.js
remote:

	mongo ds037637.mongolab.com:37637/geomatcher -u <user> -p <password> all-at-once.js

**3) export (make sure to change the fields to whatever you need them to be)**

local:
 
	mongoexport -d geomatcher -c inq -f Address,"First Name","Last Name",Number,lat,lon,session1,miles1,session2,miles2 --csv -o ~/Desktop/geomatcher-out.csv
	
remote: 

	mongoexport -h ds037637.mongolab.com:37637 -d geomatcher -c <collection> -u <user> -p <password> -o <output .csv file> --csv -f <comma-separated list of field names>

------------------------------------------------------------------

## Using the 'automagic script' all-at-once.js
All-at-once allows you (fingers crossed) to add a loc field to a new collection and match it to a sessions collection with the change of one variable, and the execution of one terminal command. See file for details.


## importing-csv.txt
contains basic commands for importing a csv file into a collection. Note that these commands need to be run from a new terminal window and not from the mongo console.

Session data is expected to take this general form, but variations can be accomodated by modifying locfield.js and calculate-distance.js:

		{
			"City" : "New Orleans",
			"Date" : "Saturday, February 23, 13",
			"Location" : "NEW ORLEANS",
			"Start Time" : "1:00 PM",
			"Venue" : "Contemporary Arts Center",
			"_id" : ObjectId("506b15b798a91000c51a9c4d"),
			"addr" : "900 Camp Street New Orleans LA 70130",
			"lat" : 29.94385,
			"loc" : {
				"lon" : -90.070674,
				"lat" : 29.94385
			},
			"lon" : -90.070674,
			"state" : "LA",
			"street" : "900 Camp Street",
			"website" : "www.cacno.org",
			"zip" : 70130
		}


## locfield.js
blueprint mongo shell script for creating an embedded "loc" object within database documents that latitute and longitute fields

## calculate-distance.js
main mongo shell script that adds nearest session information to collection. New fields will be:
session1
session2	(optional, only created if two or more sessions are within a given range)
miles1
miles2		(optional, only created if two or more sessions are within a given range)

*Once the correct collection names are set, this script is standalone.*

## Basic Commands
* Finding records: db.collectionName.find();
* Finding some records (ones with a key equal to value): db.collectionName.find({"key": "value"});
* Turning the results into an Array: db.collectionName.find({"key": "value"}).toArray();
* Stash results as a variable: var newVar = db.collectionName.find({"key": "value"}).toArray();
* Remove some records (ones with a key equal to value): db.collectionName.remove({"key": "value"});

## Using a remote database
* instead of starting mongo with "mongo" from the terminal, use "mongo url.to.database/dbName -u <dbuser> -p <dbpassword>"

## Other Notes:
* To move a collection to a different database (must be run from admin, i.e. "use admin"):
db.runCommand({renameCollection:"sourcedb.mycol",to:"targetdb.mycol"})

