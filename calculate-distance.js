// * -------------------------------------------
//	## Main Matching File
//	* setup initial location index and variables
//	* create a loop that first looks for multiple sessions within 50 miles, and then just the closest secction
// -------------------------------------------  */

// only need to change these values if you are running this through the mongo console and not through all-at-once.js
if (!mainCollection) { var mainCollection = "newinq";}
if (!sessionsCollection) {var sessionsCollection = "sessions1314";}

// make sure all sessions are (re)indexed for geospatial queries before running everything else
db[sessionsCollection].ensureIndex({loc:"2d"});

// setup earth radius for distance calculations, and the range that we are interested in
var earthRadius = 3959; // radius of the earth in miles
var range = 250; // first pass we are looking for multiple sessions within 250 miles
 var maxRange = 350; // max for matching 1 session if 2 are not within 250 miles

// -------------------------------------------
// ## Main Loop. 
// 'newinq' will need to be changed to the collection 
// name that we want to match against
// var arr = db.newinq.find().toArray();
//
// 'completeSessions' will need to be changed to the 
// name of the sessions collection
//-------------------------------------------  */
var arr = db[mainCollection].find().toArray();
for (var i=0, len = arr.length; i < len; i++) {
	if (i === len-1) {print('...success, we\'re done matching!');}
	var lon = arr[i].loc.lon;
	var lat = arr[i].loc.lat;
	// -------------------------------------------
	// ## Debug. Uncomment these lines to print out the 
	// longitude and the record number to the console as the 
	// loop is iterated through. You can also access other
	// variables and print more descriptive things, e.g.:
	// print("Hey, " + arr[i].first + " " + arr[i].last);
	//-------------------------------------------*/
	// print(lon);
	// print(i)

	//columbiaMiels

	//sessions stuff

	if (lon !== null) {
		distances = db.runCommand({ geoNear : sessionsCollection, near : [lon,lat], spherical : true, maxDistance : range / earthRadius, distanceMultiplier : earthRadius }).results;
	}
	if (distances.length >= 2) {
		// -------------------------------------------
		// ## Stashing variables for two closest sessions on their distance.
		// Note that you may need to change the field names ('Location', 'Date', 'addr',etc)
		// -------------------------------------------*/
		var session1 = distances[0].obj.Location;
		var sessionDate1 = distances[0].obj.Date;
		var sessionVenue1 = distances[0].obj.Venue;
		var sessionAddress1 = distances[0].obj.addr;
		var sessionStartTime1 = distances[0].obj["Start Time"];
		var miles1 = distances[0].dis;

		var session2 = distances[1].obj.Location;
		var sessionDate2 = distances[1].obj.Date;
		var sessionVenue2 = distances[1].obj.Venue;
		var sessionAddress2 = distances[1].obj.addr;
		var sessionStartTime2 = distances[1].obj["Start Time"];
		var miles2 = distances[1].dis;

		// now that we have everything stashed, UPDATE the COLLECTION
		db[mainCollection].update({"_id" : arr[i]["_id"]}, { $set: {
			session1 : session1,
			sessionDate1 : sessionDate1,
			sessionVenue1 : sessionVenue1,
			sessionAddress1 : sessionAddress1,
			sessionStartTime1: sessionStartTime1,
			miles1 : miles1,

			session2 : session2,
			sessionDate2 : sessionDate2,
			sessionVenue2 : sessionVenue2,
			sessionAddress2 : sessionAddress2,
			sessionStartTime2 : sessionStartTime2,
			miles2 : miles2
			}
		});
	} else {
		// -------------------------------------------
		// ## If we aren't dealing with more than one session
		// within the given range, we just want to grab the 
		// closest session.
		// -------------------------------------------*/

		nearestSession = db.runCommand({ geoNear : sessionsCollection, near : [lon, lat], spherical : true, maxDistance : 350 / earthRadius, distanceMultiplier: earthRadius, num: 1}).results;
			if (nearestSession.length > 0) {
			var session1 = nearestSession[0].obj.Location;
			var sessionDate1 = nearestSession[0].obj.Date;
			var sessionVenue1 = nearestSession[0].obj.Venue;
			var sessionAddress1 = nearestSession[0].obj.addr;
			var sessionStartTime1 = nearestSession[0].obj["Start Time"];
			var miles1 = nearestSession[0].dis;
		
			// now that we have everything stashed, UPDATE the COLLECTION
			db[mainCollection].update({"_id" : arr[i]["_id"]}, { $set: {
				session1 : session1,
				sessionDate1 : sessionDate1,
				sessionVenue1 : sessionVenue1,
				sessionAddress1 : sessionAddress1,
				sessionStartTime1 : sessionStartTime1,
				miles1: miles1
			}} );
		}
		
	}
}

// nearestSession = db.runCommand({ geoNear : sessionsCollection, near : [-66.0409,18.37992], spherical : true, maxDistance : 350 / earthRadius, distanceMultiplier: earthRadius, num: 1}).results;
