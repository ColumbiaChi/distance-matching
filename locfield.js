/* ## Create a loc field for geo-spatial queries from lat lon fields
	
   roll through each record
   for each record set the loc field to the combo of the lat and lon fields

   db.example.update({"last" : "Penn"},{$set : {loc: {lat:40, lon:40} }});
   db.example.update({"_id" : arr[0]["_id"]},{$set : {loc: { Lat : arr[0].lat, Lon : arr[0].lon} }});
*/

///////
var lat, lon = "";
var arr = db[mainCollection].find().toArray();
//count the array
var count = arr.length;
// create a loc field from lat and lon
// for (var i=0; i < count; i++) {
//     db.example.update({"_id" : arr[i]["_id"]},{$set : {loc: { Lat : arr[i].lat, Lon : arr[i].lon} }});
// }

//remeber that this won't update the array...just the db itself.


for (var i=0; i < count; i++) {
	db[mainCollection].update({"_id" : arr[i]["_id"]},{$set : {loc: { lon : arr[i].Longitude, lat : arr[i].Latitude } }});
}